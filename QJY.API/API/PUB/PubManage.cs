﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace QJY.API
{
    public class PubManage
    {



        #region 官网登录和注册




        public void CHECKREGISTERPHONE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var qy2 = new JH_Auth_QYB().GetEntities(p => p.Mobile == P1.Trim());
            if (qy2.Count() > 0)
            {
                msg.ErrorMsg = "此手机已注册企业，请更换手机号继续注册";
            }
        }



        public void REGISTERYSOLD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JH_Auth_QY QY = new JH_Auth_QYB().GetEntity(d => d.ComId == 10334);
            QY.CRDate = DateTime.Now.AddYears(-2);
            QY.QYProfile = QY.FileServerUrl;
            QY.FileServerUrl = "";
            QY.IsUseWX = "N";
            new JH_Auth_QYB().Update(QY);
            msg.Result = QY;

        }

        public void REGISTERYS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strXM = P2;
            string strPhone = P1;
            JH_Auth_User user1 = new JH_Auth_UserB().GetUserByUserName(10334, P1);
            if (user1 != null)
            {
                msg.ErrorMsg = "用户已存在";
                return;
            }
            JH_Auth_User user = new JH_Auth_User();
            user.UserName = strPhone;
            user.mobphone = strPhone;
            user.UserRealName = P2;
            user.UserPass = CommonHelp.GetMD5("abc123");
            user.ComId = 10334;
            user.BranchCode = 1728;
            user.CRDate = DateTime.Now;
            user.CRUser = "administrator";
            user.logindate = DateTime.Now;
            user.IsUse = "Y";
            if (!new JH_Auth_UserB().Insert(user))
            {
                msg.ErrorMsg = "添加用户失败";
            }
            else
            {

                JH_Auth_QY QY = new JH_Auth_QYB().GetEntity(d => d.ComId == 10334);
                WXHelp wx = new WXHelp(QY);
                wx.WX_CreateUser(user);

                //添加默认员工角色
                JH_Auth_UserRole Model = new JH_Auth_UserRole();
                Model.UserName = user.UserName;
                Model.RoleCode = 1219;
                Model.ComId = user.ComId;
                new JH_Auth_UserRoleB().Insert(Model);

            }
        }
        /// <summary>
        /// 发送手机验证码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SENDCHKMSG(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //if (!string.IsNullOrEmpty(P1))
            //{
            //    string code = CommonHelp.numcode(4);
            //    try
            //    {
            //        string type = context.Request["type"] ?? "";
            //        string content = "";
            //        switch (type)
            //        {
            //            case "changeadmin":
            //                content = "您更换超级管理员的验证码为：" + code + "，如非本人操作，请忽略本短信";
            //                break;
            //            default:
            //                content = "注册验证码：" + code + "，如非本人操作，请忽略本短信";
            //                break;

            //        }

            //        CommonHelp.SendSMS(P1, content, 0);
            //        msg.Result = CommonHelp.GetMD5(code);
            //    }
            //    catch
            //    {
            //        msg.ErrorMsg = "发送验证码失败";
            //    }
            //}
        }
        /// <summary>
        /// 验证企业名称
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void YZQYMC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(P1))
            {
                var qy = new JH_Auth_QYB().GetEntity(p => p.QYName == P1);
                if (qy != null)
                {
                    msg.ErrorMsg = "企业名称已存在";
                }
            }
        }


        #endregion


        #region 评论


        /// <summary>
        /// 获取企业信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETQYINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string qycode = context.Request("qycode");
            //string qycode = "2072782222";
            JH_Auth_QY Qyinfo = new JH_Auth_QYB().GetEntity(d => d.QYCode == qycode);
            if (Qyinfo == null)
            {
                msg.ErrorMsg = "没有找到该企业";
            }
            else
            {
                msg.Result = Qyinfo;


                //WXHelp wx = new WXHelp(Qyinfo);
                //var list = wx.GetAppList().agentlist.Select(p => new { id = p.agentid, info = wx.GetAPPinfo(Int32.Parse(p.agentid)) });
                //wx.WX_WxCreateMenuNew(138, "GZBG");
                //msg.Result1 = list;
            }
        }
        #endregion



        #region 找回密码
        public void CHECKPHONE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //string ComId = context.Request["comId"] ?? "";
            //int id = 0;
            //int.TryParse(ComId, out id);
            //if (id > 0)
            //{
            //    List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.mobphone == P2 && d.ComId == id).ToList();
            //    JH_Auth_User user = new JH_Auth_UserB().GetEntity(d => d.mobphone == P2 && d.ComId == id);
            //    if (userList.Count != 1)
            //    {
            //        msg.ErrorMsg = "此手机号无效";
            //    }
            //}
            //else
            //{
            //    List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.mobphone == P2).ToList();
            //    if (userList.Count == 0)
            //    {
            //        msg.ErrorMsg = "此手机号无效";
            //    }
            //    else if (userList.Count > 1)
            //    {
            //        msg.ErrorMsg = "-1";
            //    }
            //}

        }
        //找回密码验证二级域名
        public void FINDYZQYYM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //if (!string.IsNullOrEmpty(P1))
            //{
            //    if (P1.ToLower() == "www" || P1.ToLower() == "saas") //www及saas不让用户注册
            //    {
            //        msg.ErrorMsg = "二级域名无效";
            //        return;
            //    }
            //    var qy = new JH_Auth_QYB().GetEntity(p => p.QYCode == P1);
            //    if (qy == null)
            //    {
            //        msg.ErrorMsg = "二级域名不存在";
            //    }
            //    else
            //    {
            //        msg.Result = qy.ComId;
            //    }
            //}
        }
        public void FINDPASSWORD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //string userpass = context.Request["pass"];
            //if (P1 == "1")
            //{
            //    string ComId = context.Request["ComId"];
            //    int qyId = 0;
            //    int.TryParse(ComId, out qyId);
            //    JH_Auth_QY qymodel = new JH_Auth_QYB().GetEntity(d => d.ComId == qyId);
            //    if (qymodel != null)
            //    {
            //        JH_Auth_User userInfo = new JH_Auth_UserB().GetEntity(d => d.mobphone == P2 && d.ComId == qymodel.ComId);
            //        userInfo.UserPass = CommonHelp.GetMD5(userpass);
            //        new JH_Auth_UserB().Update(userInfo);
            //        msg.Result = qymodel.QYCode;
            //    }
            //}
            //else
            //{
            //    JH_Auth_User userInfo = new JH_Auth_UserB().GetEntity(d => d.mobphone == P2);
            //    userInfo.UserPass = CommonHelp.GetMD5(userpass);
            //    new JH_Auth_UserB().Update(userInfo);
            //}
        }
        #endregion




        public void GETBDTJDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int pdid = 0;
            int.TryParse(P1, out pdid);


            string strSDate = context.Request("sdate") ?? DateTime.Now.AddYears(-20).ToString("yyyy-MM-dd");
            string strEDate = context.Request("edate") ?? DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");


            List<JH_Auth_ExtendMode> ExtendModes = new List<JH_Auth_ExtendMode>();
            ExtendModes = new JH_Auth_ExtendModeB().GetEntities(D => D.PDID == pdid).ToList();
            string strWhere = "";
            if (P2 != "")
            {
                JArray datas = (JArray)JsonConvert.DeserializeObject(P2);
                foreach (JObject item in datas)
                {
                    string filed = (string)item["filed"];
                    //if (ExtendModes.Select(D => D.TableFiledColumn).ToList().Contains(filed))
                    //{
                    string qtype = (string)item["qtype"];
                    string qvalue = (string)item["qvalue"];
                    strWhere = CommonHelp.CreateqQsql(filed, qtype, qvalue);
                    // }

                }
            }
            string strISGD = context.Request("isGD") ?? "";
            if (strISGD != "")
            {
                strWhere = strWhere + " AND ISGD='" + strISGD.FilterSpecial() + "'";
            }


            string pdfields = context.Request("pdfields") ?? "";
            if (pdfields != "")
            {
                ExtendModes = ExtendModes.Where(d => d.TableFiledColumn == pdfields).ToList();
            }
            if (ExtendModes.Count > 0)
            {
                string strTempSQL = new Yan_WF_PDB().GetDTHZL(ExtendModes.Select(D => D.TableFiledColumn).ToList().ListTOString(','), pdid.ToString());
                string strSQL = strTempSQL + " WHERE CRDATE BETWEEN '" + strSDate + " 01:01:01'  AND '" + strEDate + " 23:59:59' " + strWhere;
                DataTable dt = new Yan_WF_PDB().GetDTByCommand(strSQL);
                msg.Result = dt;
                msg.Result1 = ExtendModes;
            }

        }


        /// <summary>
        /// 获取仪表盘可用Token
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYBPTOKEN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            new DATABIManage().GETYBDATA(context, msg, P1, P2, UserInfo);
        }

        public void GETINITDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY Model = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
            Model.TempCode = (int.Parse(Model.TempCode) + 1).ToString();
            new JH_Auth_QYB().Update(Model);
            msg.Result = Model.TempCode;

            msg.Result1 = new JH_Auth_QYB().GetDTByCommand("select id from yan_wf_pi where PDID IN ('242','244')").Rows.Count;

        }


        




    }
}